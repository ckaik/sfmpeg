# SFmpeg

This package aims to provide a way to utilize FFmpeg within Swift. 

At this point, the package can only be used to build FFmpeg filter graphs using the `FilterComplex` type and Swift's Resuilt Builders.
There is no intelligence in the ResultBuilders (yet?), e.g. at some point things like automated adding of `split` filters could be added.

⚠️ I currently have no practical use for this and there's no documentation and basically no tests.
Right now it's just a project like to tinker with in my free time and if it ever gets somewhere, great!

## Filter Graph Example

Here's some sample code that generates a filter graph for outputs specified in the variables in the beginning.
This filtergraph should work just fine when put into ffmpeg.

```
[0:0]scale=width=3840:height=2160:force_divisible_by=2,split=2[sdr][hdr];[sdr]zscale=transfer=linear:npl=100,format=gbrpf32le,zscale=primaries=709,tonemap=tonemap=hable:desat=0.0,zscale=transfer=709:matrix=709:range=limited,format=yuv420p,split=3[v-2160p-sdr][v-1080p-sdr][v-540p-sdr];[hdr]format=yuv420p10le,split=3[v-2160p-hdr][v-1080p-hdr][v-540p-hdr];[v-2160p-sdr]drawtext=text='UHD':fontcolor=#FFFFFFFF:fontsize=75:box=1:boxcolor=#0000007F:borderw=20:x=20:y=20[v-2160p-sdr];[v-2160p-hdr]drawtext=text='UHD':fontcolor=#FFFFFFFF:fontsize=75:box=1:boxcolor=#0000007F:borderw=20:x=20:y=20[v-2160p-hdr];[v-1080p-sdr]scale=width=1920:height=1080:force_original_aspect_ratio=decrease:force_divisible_by=2,drawtext=text='Full HD':fontcolor=#FFFFFFFF:fontsize=37:box=1:boxcolor=#0000007F:borderw=10:x=10:y=10[v-1080p-sdr];[v-1080p-hdr]scale=width=1920:height=1080:force_original_aspect_ratio=decrease:force_divisible_by=2,drawtext=text='Full HD':fontcolor=#FFFFFFFF:fontsize=37:box=1:boxcolor=#0000007F:borderw=10:x=10:y=10[v-1080p-hdr];[v-540p-sdr]scale=width=960:height=540:force_original_aspect_ratio=decrease:force_divisible_by=2,drawtext=text='SD':fontcolor=#FFFFFFFF:fontsize=19:box=1:boxcolor=#0000007F:borderw=5:x=5:y=5[v-540p-sdr];[v-540p-hdr]scale=width=960:height=540:force_original_aspect_ratio=decrease:force_divisible_by=2,drawtext=text='SD':fontcolor=#FFFFFFFF:fontsize=19:box=1:boxcolor=#0000007F:borderw=5:x=5:y=5[v-540p-hdr]
```

```swift
struct Output {
	var name: String
	
	var width: Int
	var height: Int
	
	var text: String
	var borderWidth: Int
	var fontSize: Int
	
	var hdrName: String { name + "-hdr" }
	var sdrName: String { name + "-sdr" }
	
	var resolution: Int { width * height }
}

let inputWidth = 7680
let inputHeight = 4320

let outputs = [
	Output(
		name: "v-2160p", 
		width: 3840, 
		height: 2160, 
		text: "UHD", 
		borderWidth: 20, 
		fontSize: 75
	),
	Output(
		name: "v-1080p", 
		width: 1920, 
		height: 1080, 
		text: "Full HD", 
		borderWidth: 10, 
		fontSize: 37
	),
	Output(
		name: "v-540p",
		width: 960,
		height: 540,
		text: "SD",
		borderWidth: 5,
		fontSize: 19
	)
]

var highestRes = Int.min
var highestOutput: Output = outputs.first!

for output in outputs where output.resolution > highestRes {
	highestRes = output.resolution
	highestOutput = output
}

let complex = FilterComplex {
	// 1. Pre-Scale the Video if not yet at the highest resolution we need
	// In addition, split for SDR / HDR Pipelines
	FilterChain(inputs: "0:0", outputs: "sdr", "hdr") {
		if highestOutput.width != inputWidth || highestOutput.height != inputHeight {
			Scale(width: highestOutput.width, height: highestOutput.height)
				.forceDivisibleBy(2)
		}

		Split(2)
	}
	
	// 2. Tonemap SDR Footage and split it for all appropriately
	FilterChain(inputs: "sdr", outputs: outputs.map(\.sdrName)) { 
		ZScale()
			.colorTransfer(.linear)
			.nominalPeakLuminance(100)

		Format(.gbrpf32le)

		ZScale()
			.colorPrimaries(.bt709)

		Tonemap(.hable)
			.desaturate(0)

		ZScale()
			.colorTransfer(.bt709)
			.colorMatrix(.bt709)
			.colorRange(.limited)

		Format(.yuv420p)
		
		if outputs.count > 1 {
			Split(outputs.count)
		}
	}

	// 3. For HDR, set a 10-bit Pixel Format and split it appropriately
	FilterChain(inputs: "hdr", outputs: outputs.map(\.hdrName)) {
		Format(.yuv420p10le)
		
		if outputs.count > 1 {
			Split(outputs.count)
		}
	}
	
	// 4. Add the Text Overlay
	for output in outputs {
		for name in [output.sdrName, output.hdrName] {
			FilterChain(inputs: name, outputs: name) {
				// 4.1. - If the output's size is not already at the highest resolution, add a scale
				if output.width != highestOutput.width || output.height != highestOutput.height {
					Scale(width: output.width, height: output.height)
						.forceOriginalAspectRatio(.decrease)
						.forceDivisibleBy(2)
				}
				
				// 4.2. - Draw Text
				DrawText(output.text)
					.fontColor(OSColor.white)
					.fontSize(output.fontSize)
					.box()
					.boxColor(OSColor.black.withAlphaComponent(0.5))
					.borderWidth(output.borderWidth)
					.x(output.borderWidth)
					.y(output.borderWidth)
			}	
		}
	}
}

// Calling ffmpegString on the FilterComplex will return the filter string for use in ffmpeg
print(complex.ffmpegString)
```
