import Foundation

public protocol FilterProtocol: FFmpegStringConvertible, FilterConvertible {
	var name: String { get }
	var arguments: [FilterArgument] { get set }
}

public protocol FilterConvertible {
	func filter() -> [FilterProtocol]
}

extension Array: FilterConvertible where Element == FilterProtocol {
	public func filter() -> [FilterProtocol] { self }
}

public extension FilterProtocol {
	var ffmpegString: String {
		if arguments.count > 0 {
			return "\(name)=\(arguments.map(\.ffmpegString).joined(separator: ":"))"
		}

		return name
	}

	func filter() -> [FilterProtocol] {
		[self]
	}
}
