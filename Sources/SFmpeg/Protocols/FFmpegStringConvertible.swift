import Foundation

public protocol FFmpegStringConvertible {
	var ffmpegString: String { get }
}
