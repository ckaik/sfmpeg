import Foundation

public protocol FilterArgument: FFmpegStringConvertible {
	var name: String { get }
	var valueString: String { get }
}

public extension FilterArgument {
	var ffmpegString: String {
		if name.isEmpty {
			return valueString
		} else {
			return "\(name)=\(valueString)"
		}
	}
}
