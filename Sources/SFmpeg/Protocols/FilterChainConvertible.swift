import Foundation

public protocol FilterChainConvertible {
	func filterChain() -> [FilterChain]
}

extension Array: FilterChainConvertible where Element == FilterChain {
	public func filterChain() -> [FilterChain] { self }

	public init(@FilterComplexBuilder _ content: () -> FilterComplex) {
		self.init(content())
	}
}

extension FilterChain: FilterChainConvertible {
	public func filterChain() -> [FilterChain] { [self] }
}
