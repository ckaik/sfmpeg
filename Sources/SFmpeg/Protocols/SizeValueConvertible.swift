import Foundation

public protocol SizeValueConvertible {
	var sizeValue: SizeValue { get }
}
