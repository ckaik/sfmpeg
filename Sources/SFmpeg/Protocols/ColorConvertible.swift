import Foundation

#if canImport(UIKit)
	import UIKit
#elseif canImport(AppKit)
	import AppKit
#endif

public protocol FFmpegColorConvertible: FFmpegStringConvertible {
	func toFFmpegColor() -> FFmpegColor
}

public extension FFmpegColorConvertible {
	var ffmpegString: String { toFFmpegColor().ffmpegString }
}

#if canImport(UIKit)
	public typealias OSColor = UIColor
#elseif canImport(AppKit)
	public typealias OSColor = NSColor
#endif

#if canImport(UIKit) || canImport(AppKit)
	extension OSColor: FFmpegColorConvertible {
		public func toFFmpegColor() -> FFmpegColor {
			let color = CIColor(color: self)!

			return FFmpegColor(red: UInt8(color.red * 255),
			                   green: UInt8(color.green * 255),
			                   blue: UInt8(color.blue * 255),
			                   alpha: UInt8(color.alpha * 255))
		}
	}
#endif
