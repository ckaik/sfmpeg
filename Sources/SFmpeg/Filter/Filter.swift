import Foundation

open class Filter: FilterProtocol {
	public var name: String
	public var arguments: [FilterArgument]

	public init(name: String, arguments: [FilterArgument] = []) {
		self.name = name
		self.arguments = arguments
	}

	public func argument(_ argument: FilterArgument) -> Self {
		let index = arguments.firstIndex { $0.name == argument.name }

		if let index = index {
			arguments.remove(at: index)
		}

		arguments += argument

		return self
	}

	public func argument(name: String, value: any FFmpegStringConvertible) -> Self {
		argument(Argument(name: name, value: value))
	}
}
