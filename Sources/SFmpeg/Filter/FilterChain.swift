import Foundation

public struct FilterChain: FFmpegStringConvertible {
	public var inputs: [String]
	public var outputs: [String]

	public var filters: [FilterProtocol]

	public init(inputs: [String], outputs: [String], @FilterChainBuilder content: () -> [FilterProtocol]) {
		self.inputs = inputs
		self.outputs = outputs
		self.filters = content()
	}

	public init(inputs: String..., outputs: String..., @FilterChainBuilder content: () -> [FilterProtocol]) {
		self.init(inputs: inputs, outputs: outputs, content: content)
	}

	public init(inputs: [String], outputs: String..., @FilterChainBuilder content: () -> [FilterProtocol]) {
		self.init(inputs: inputs, outputs: outputs, content: content)
	}

	public init(inputs: String..., outputs: [String], @FilterChainBuilder content: () -> [FilterProtocol]) {
		self.init(inputs: inputs, outputs: outputs, content: content)
	}

	public var ffmpegString: String {
		let inputs = inputs.map(\.ffmpegIoFormatted).joined()
		let outputs = outputs.map(\.ffmpegIoFormatted).joined()
		let filters = filters.map(\.ffmpegString).joined(separator: ",")

		return "\(inputs)\(filters)\(outputs)"
	}
}

@resultBuilder
public enum FilterChainBuilder {
	public static func buildBlock(_ arguments: FilterConvertible...) -> [FilterProtocol] {
		arguments.flatMap { $0.filter() }
	}

	public static func buildIf(_ value: FilterConvertible?) -> FilterConvertible {
		value ?? []
	}

	public static func buildEither(first: FilterConvertible) -> FilterConvertible {
		first
	}

	public static func buildEither(second: FilterConvertible) -> FilterConvertible {
		second
	}

	public static func buildArray(_ components: [[FilterProtocol]]) -> [FilterProtocol] {
		components.flatMap { $0 }
	}
}
