import Foundation

public class Tonemap: Filter {
	public init(_ algorithm: Algorithm = .none) {
		super.init(name: "tonemap")

		arguments += Argument(name: "tonemap", value: algorithm.rawValue)
	}

	public func parameter(_ value: Double) -> Self {
		argument(name: "param", value: value)
	}

	public func desaturate(_ value: Double) -> Self {
		argument(name: "desat", value: value)
	}

	public func peak(_ value: Int) -> Self {
		argument(name: "peak", value: value)
	}
}

public extension Tonemap {
	enum Algorithm: String, FFmpegStringConvertible {
		case none
		case clip
		case linear
		case gamma
		case reinhard
		case hable
		case mobius
	}
}
