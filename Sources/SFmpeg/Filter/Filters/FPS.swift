import Foundation

public class FPS: Filter {
	public init(_ fps: FrameRate = .sourceFPS) {
		super.init(name: "fps")

		self.arguments = [Argument(value: fps)]
	}

	public init(_ fps: Double) {
		super.init(name: "fps")

		self.arguments = [Argument(value: fps)]
	}

	public func roundingBehavior(_ behavior: RoundingBehavior) -> Self {
		argument(name: "round", value: behavior)
	}

	public func endOfFileAction(_ action: EndOfFileAction) -> Self {
		argument(name: "eof_action", value: action)
	}

	public func startTime(_ time: Int) -> Self {
		argument(name: "start_time", value: time)
	}
}

public extension FPS {
	enum EndOfFileAction: String, FFmpegStringConvertible {
		case round
		case pass
	}
}
