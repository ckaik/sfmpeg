import Foundation

public class ZScale: Scale {
	override public init(width: SizeValueConvertible? = nil, height: SizeValueConvertible? = nil) {
		super.init()

		self.name = "zscale"
	}

	public func chromaLocation(_ value: ChromaLocation) -> Self {
		argument(name: "chromal", value: value)
	}

	public func inputChromaLocation(_ value: ChromaLocation) -> Self {
		argument(name: "chromalin", value: value)
	}

	public func colorMatrix(_ value: ColorMatrix) -> Self {
		argument(name: "matrix", value: value)
	}

	public func inputColorMatrix(_ value: ColorMatrix) -> Self {
		argument(name: "matrixin", value: value)
	}

	public func colorPrimaries(_ value: ColorPrimaries) -> Self {
		argument(name: "primaries", value: value)
	}

	public func inputColorPrimaries(_ value: ColorPrimaries) -> Self {
		argument(name: "primariesin", value: value)
	}

	public func colorTransfer(_ value: ColorTransferOutput) -> Self {
		argument(name: "transfer", value: value)
	}

	public func inputColorTransfer(_ value: ColorTransferInput) -> Self {
		argument(name: "transferin", value: value)
	}

	public func colorRange(_ value: ColorRange) -> Self {
		argument(name: "range", value: value)
	}

	public func inputColorRange(_ value: ColorRange) -> Self {
		argument(name: "rangein", value: value)
	}

	public func filter(_ value: Filter) -> Self {
		argument(name: "filter", value: value)
	}

	public func dither(_ value: Dither) -> Self {
		argument(name: "dither", value: value)
	}

	public func paramA(_ value: String) -> Self {
		argument(name: "param_a", value: value)
	}

	public func paramB(_ value: String) -> Self {
		argument(name: "param_b", value: value)
	}

	public func nominalPeakLuminance(_ value: Int) -> Self {
		argument(name: "npl", value: value)
	}
}

public extension ZScale {
	enum ColorRange: String, FFmpegStringConvertible {
		case input
		case limited
		case full
	}

	enum ChromaLocation: String, FFmpegStringConvertible {
		case input
		case left
		case center
		case topleft
		case top
		case bottomleft
		case bottom
	}

	enum ColorMatrix: String, FFmpegStringConvertible {
		case input

		case bt709 = "709"
		case bt470bg = "470bg"
		case bt170m = "170m"
		case bt2020_ncl = "2020_ncl"
		case bt2020_cl = "2020_cl"

		case unspecified
	}

	enum ColorPrimaries: String, FFmpegStringConvertible {
		case input

		case bt709 = "709"
		case bt170m = "170m"
		case bt240m = "240m"
		case bt2020 = "2020"

		case unspecified
	}

	enum ColorTransferOutput: String, FFmpegStringConvertible {
		case input
		case linear

		case bt709 = "709"
		case bt601 = "601"
		case bt2020_10 = "2020_10"
		case bt2020_12 = "2020_12"
		case smpte2084
		case iec6196621 = "iec61966-2-1"
		case aribStdB67 = "arib-std-b67"

		case unspecified
	}

	enum ColorTransferInput: String, FFmpegStringConvertible {
		case input
		case linear

		case bt709 = "709"
		case bt601 = "601"
		case bt2020_10 = "2020_10"
		case bt2020_12 = "2020_12"

		case unspecified
	}

	enum Dither: String, FFmpegStringConvertible {
		case none
		case ordered
		case random
		case errorDiffusion = "error_diffusion"
	}

	enum Filter: String, FFmpegStringConvertible {
		case point
		case bilinear
		case bicubic
		case spline16
		case spline36
		case lanczos
	}
}
