import Foundation

public class ScaleNPP: Scale {
	override public init(width: SizeValueConvertible? = nil, height: SizeValueConvertible? = nil) {
		super.init()

		self.name = "scale_npp"
	}

	public func interpolationAlgorithm(_ value: InterpolationAlgorithm) -> Self {
		argument(name: "interp_algo", value: value)
	}
}

public extension ScaleNPP {
	enum InterpolationAlgorithm: String, FFmpegStringConvertible {
		case nearestNeighbor = "nn"
		case linear
		case cubic
		case cubic2pBSpline = "cubic2p_bspline"
		case cubic2pCatmullRom = "cubic2p_catmullrom"
		case cubic2pB05C03 = "cubic2p_b05c03"
		case supersampling = "super"
		case lanczos
	}
}
