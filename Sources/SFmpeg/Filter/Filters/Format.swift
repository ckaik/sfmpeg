import Foundation

public class Format: Filter {
	public init(_ formats: PixelFormat...) {
		super.init(name: "format")

		arguments += Argument(value: formats.map(\.rawValue).joined(separator: "|"))
	}
}
