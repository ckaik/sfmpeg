import Foundation

public class Scale: Filter {
	public init(width: SizeValueConvertible? = nil, height: SizeValueConvertible? = nil) {
		super.init(name: "scale")

		if let width = width?.sizeValue {
			arguments += Argument(name: "width", value: width)
		}

		if let height = height?.sizeValue {
			arguments += Argument(name: "height", value: height)
		}
	}

	public func forceOriginalAspectRatio(_ value: ForceOriginalAspectRatioMode) -> Self {
		argument(name: "force_original_aspect_ratio", value: value)
	}

	public func forceDivisibleBy(_ value: UInt) -> Self {
		argument(name: "force_divisible_by", value: value)
	}
}
