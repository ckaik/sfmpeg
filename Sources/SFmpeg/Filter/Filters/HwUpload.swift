import Foundation

public class HwUpload: Filter {
	public init(_ deviceType: String? = nil) {
		super.init(name: "hwupload")

		if let deviceType = deviceType {
			arguments += Argument(name: "derive_device", value: deviceType)
		}
	}
}
