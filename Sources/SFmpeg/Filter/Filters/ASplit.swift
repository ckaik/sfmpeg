import Foundation

public class ASplit: Filter {
	public init(_ numberOfOutputs: Int) {
		super.init(name: "asplit")

		arguments += Argument(value: abs(numberOfOutputs))
	}
}
