import Foundation

public class Split: Filter {
	public init(_ numberOfOutputs: Int) {
		super.init(name: "split")

		arguments += Argument(value: abs(numberOfOutputs))
	}
}
