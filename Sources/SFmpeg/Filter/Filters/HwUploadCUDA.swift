import Foundation

public class HwUploadCUDA: Filter {
	public init(_ deviceNumber: UInt? = nil) {
		super.init(name: "hwupload_cuda")

		if let deviceNumber = deviceNumber {
			arguments += Argument(name: "device", value: deviceNumber)
		}
	}
}
