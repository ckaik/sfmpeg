import Foundation

public class DrawText: Filter {
	public init(_ text: String) {
		super.init(name: "drawtext")

		self.arguments = [Argument(name: "text", value: "'\(text)'")]
	}

	public func box(_ value: Bool = true) -> Self {
		argument(name: "box", value: value)
	}

	public func baseTime(_ value: Int) -> Self {
		argument(name: "basetime", value: value)
	}

	public func borderWidth(_ value: Int) -> Self {
		argument(name: "borderw", value: value)
	}

	public func boxBorderWidth(_ value: Int) -> Self {
		argument(name: "boxborderw", value: value)
	}

	public func borderColor(_ value: any FFmpegColorConvertible) -> Self {
		argument(name: "bordercolor", value: value)
	}

	public func boxColor(_ value: any FFmpegColorConvertible) -> Self {
		argument(name: "boxcolor", value: value)
	}

	public func expansion(_ value: Expansion) -> Self {
		argument(name: "expansion", value: value)
	}

	public func lineSpacing(_ value: Int) -> Self {
		argument(name: "line_spacing", value: value)
	}

	public func fixBounds(_ value: Bool) -> Self {
		argument(name: "fix_bounds", value: value)
	}

	public func fontColor(_ value: any FFmpegColorConvertible) -> Self {
		argument(name: "fontcolor", value: value)
	}

	public func fontColorExpression(_ value: String) -> Self {
		argument(name: "fontcolor_expr", value: value)
	}

	public func font(_ value: String) -> Self {
		argument(name: "font", value: value)
	}

	public func fontFile(_ value: String) -> Self {
		argument(name: "fontfile", value: value)
	}

	public func alpha(_ value: Double) -> Self {
		argument(name: "alpha", value: min(1, max(0, value)))
	}

	public func fontSize(_ value: Int) -> Self {
		argument(name: "fontsize", value: value)
	}

	public func textShaping(_ value: Int) -> Self {
		argument(name: "text_shaping", value: value)
	}

	public func fontLoadFlags(_ value: FontLoadFlag...) -> Self {
		argument(name: "text_shaping",
		         value: value.map(\.ffmpegString).joined(separator: "|"))
	}

	public func shadowColor(_ value: any FFmpegColorConvertible) -> Self {
		argument(name: "shadowcolor", value: value)
	}

	public func x(_ value: Int) -> Self {
		argument(name: "x", value: value)
	}

	public func y(_ value: Int) -> Self {
		argument(name: "y", value: value)
	}

	public func x(_ value: String) -> Self {
		argument(name: "x", value: value)
	}

	public func y(_ value: String) -> Self {
		argument(name: "y", value: value)
	}

	public func shadowX(_ value: Int) -> Self {
		argument(name: "shadowx", value: value)
	}

	public func shadowY(_ value: Int) -> Self {
		argument(name: "shadowy", value: value)
	}

	public func shadowX(_ value: String) -> Self {
		argument(name: "shadowx", value: value)
	}

	public func shadowY(_ value: String) -> Self {
		argument(name: "shadowy", value: value)
	}

	public func startNumber(_ value: Int) -> Self {
		argument(name: "start_number", value: value)
	}

	public func tabSize(_ value: Int) -> Self {
		argument(name: "tabsize", value: value)
	}

	public func timecode(_ value: String, rate: Double) -> Self {
		argument(name: "timecode", value: value)
			.argument(name: "timecode_rate", value: rate)
	}

	public func tc24hMax(_ value: Bool) -> Self {
		argument(name: "tc24hmax", value: value)
	}
}

public extension DrawText {
	enum Expansion: String, FFmpegStringConvertible {
		case none
		case strftime
		case normal
	}
}
