import Foundation

public typealias FilterComplex = [FilterChain]

extension Array: FFmpegStringConvertible where Element == FilterChain {
	public var ffmpegString: String {
		map(\.ffmpegString).joined(separator: ";")
	}
}

@resultBuilder
public enum FilterComplexBuilder {
	public static func buildBlock(_ arguments: FilterChainConvertible...) -> FilterComplex {
		arguments.flatMap { $0.filterChain() }
	}

	public static func buildIf(_ value: FilterChainConvertible?) -> FilterChainConvertible {
		value ?? []
	}

	public static func buildEither(first: FilterChainConvertible) -> FilterChainConvertible {
		first
	}

	public static func buildEither(second: FilterChainConvertible) -> FilterChainConvertible {
		second
	}

	public static func buildArray(_ components: [FilterComplex]) -> FilterComplex {
		components.flatMap { $0 }
	}
}
