import Foundation

public enum RoundingBehavior: String, FFmpegStringConvertible {
	case down
	case up
	case towards0 = "zero"
	case awayFrom0 = "inf"
	case nearest = "near"
}
