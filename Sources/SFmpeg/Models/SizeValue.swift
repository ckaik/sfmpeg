import Foundation

public enum SizeValue: FFmpegStringConvertible {
	case sameAsInput
	case keepingAspectRatioDivisibleBy(UInt)
	case value(UInt)
	case raw(String)

	public var ffmpegString: String {
		switch self {
		case .sameAsInput:
			return "0"
		case let .keepingAspectRatioDivisibleBy(n):
			if n == 0 {
				return "0"
			}

			return "-\(n)"
		case let .value(n):
			return "\(n)"
		case let .raw(value):
			return value
		}
	}
}

extension SizeValue: SizeValueConvertible {
	public var sizeValue: SizeValue { self }
}
