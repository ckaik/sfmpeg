import Foundation

public enum PixelFormat: String, FFmpegStringConvertible {
	case _0bgr = "0bgr"
	case _0rgb = "0rgb"
	case abgr
	case argb
	case ayuv64be
	case ayuv64le
	case bayerBggr16be = "bayer_bggr16be"
	case bayerBggr16le = "bayer_bggr16le"
	case bayerBggr8 = "bayer_bggr8"
	case bayerGbrg16be = "bayer_gbrg16be"
	case bayerGbrg16le = "bayer_gbrg16le"
	case bayerGbrg8 = "bayer_gbrg8"
	case bayerGrbg16be = "bayer_grbg16be"
	case bayerGrbg16le = "bayer_grbg16le"
	case bayerGrbg8 = "bayer_grbg8"
	case bayerRggb16be = "bayer_rggb16be"
	case bayerRggb16le = "bayer_rggb16le"
	case bayerRggb8 = "bayer_rggb8"
	case bgr0
	case bgr24
	case bgr4
	case bgr444be
	case bgr444le
	case bgr48be
	case bgr48le
	case bgr4Byte = "bgr4_byte"
	case bgr555be
	case bgr555le
	case bgr565be
	case bgr565le
	case bgr8
	case bgra
	case bgra64be
	case bgra64le
	case cuda
	case d3d11
	case d3d11vaVLD = "d3d11va_vld"
	case drmPrime = "drm_prime"
	case dxva2VLD = "dxva2_vld"
	case gbrap
	case gbrap10be
	case gbrap10le
	case gbrap12be
	case gbrap12le
	case gbrap16be
	case gbrap16le
	case gbrapf32be
	case gbrapf32le
	case gbrp
	case gbrp10be
	case gbrp10le
	case gbrp12be
	case gbrp12le
	case gbrp14be
	case gbrp14le
	case gbrp16be
	case gbrp16le
	case gbrp9be
	case gbrp9le
	case gbrpf32be
	case gbrpf32le
	case gray
	case gray10be
	case gray10le
	case gray12be
	case gray12le
	case gray14be
	case gray14le
	case gray16be
	case gray16le
	case gray9be
	case gray9le
	case grayf32be
	case grayf32le
	case mediacodec
	case mmal
	case monob
	case monow
	case nv12
	case nv16
	case nv20be
	case nv20le
	case nv21
	case nv24
	case nv42
	case opencl
	case p010be
	case p010le
	case p016be
	case p016le
	case p210be
	case p210le
	case p216be
	case p216le
	case p410be
	case p410le
	case p416be
	case p416le
	case pal8
	case qsv
	case rgb0
	case rgb24
	case rgb4
	case rgb444be
	case rgb444le
	case rgb48be
	case rgb48le
	case rgb4Byte = "rgb4_byte"
	case rgb555be
	case rgb555le
	case rgb565be
	case rgb565le
	case rgb8
	case rgba
	case rgba64be
	case rgba64le
	case uyvy422
	case uyyvyy411
	case vaapi
	case vdpau
	case videotoolboxV = "videotoolbox_v"
	case vulkan
	case x2bgr10be
	case x2bgr10le
	case x2rgb10be
	case x2rgb10le
	case xvmc
	case xyz12be
	case xyz12le
	case y210be
	case y210le
	case ya16be
	case ya16le
	case ya8
	case yuv410p
	case yuv411p
	case yuv420p
	case yuv420p10be
	case yuv420p10le
	case yuv420p12be
	case yuv420p12le
	case yuv420p14be
	case yuv420p14le
	case yuv420p16be
	case yuv420p16le
	case yuv420p9be
	case yuv420p9le
	case yuv422p
	case yuv422p10be
	case yuv422p10le
	case yuv422p12be
	case yuv422p12le
	case yuv422p14be
	case yuv422p14le
	case yuv422p16be
	case yuv422p16le
	case yuv422p9be
	case yuv422p9le
	case yuv440p
	case yuv440p10be
	case yuv440p10le
	case yuv440p12be
	case yuv440p12le
	case yuv444p
	case yuv444p10be
	case yuv444p10le
	case yuv444p12be
	case yuv444p12le
	case yuv444p14be
	case yuv444p14le
	case yuv444p16be
	case yuv444p16le
	case yuv444p9be
	case yuv444p9le
	case yuva420p
	case yuva420p10be
	case yuva420p10le
	case yuva420p16be
	case yuva420p16le
	case yuva420p9be
	case yuva420p9le
	case yuva422p
	case yuva422p10be
	case yuva422p10le
	case yuva422p12be
	case yuva422p12le
	case yuva422p16be
	case yuva422p16le
	case yuva422p9be
	case yuva422p9le
	case yuva444p
	case yuva444p10be
	case yuva444p10le
	case yuva444p12be
	case yuva444p12le
	case yuva444p16be
	case yuva444p16le
	case yuva444p9be
	case yuva444p9le
	case yuvj411p
	case yuvj420p
	case yuvj422p
	case yuvj440p
	case yuvj444p
	case yuyv422
	case yvyu422
}
