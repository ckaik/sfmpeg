import Foundation

public struct FFmpegColor {
	public var red: UInt8
	public var green: UInt8
	public var blue: UInt8
	public var alpha: UInt8?

	public init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8? = nil) {
		self.red = red
		self.green = green
		self.blue = blue
		self.alpha = alpha
	}
}

extension FFmpegColor: FFmpegStringConvertible {
	public var ffmpegString: String {
		let r = String(format: "%02X", red)
		let g = String(format: "%02X", green)
		let b = String(format: "%02X", blue)

		var result = "#\(r)\(g)\(b)"

		if let alpha = alpha {
			result += String(format: "%02X", alpha)
		}

		return result
	}
}

extension FFmpegColor: FFmpegColorConvertible {
	public func toFFmpegColor() -> FFmpegColor { self }
}
