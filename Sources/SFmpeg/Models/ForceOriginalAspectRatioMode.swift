import Foundation

public enum ForceOriginalAspectRatioMode: String, FFmpegStringConvertible {
	case disable
	case decrease
	case increase
}
