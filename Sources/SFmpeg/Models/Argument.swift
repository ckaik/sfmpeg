import Foundation

public struct Argument: FilterArgument {
	public let name: String
	public let value: FFmpegStringConvertible

	public var valueString: String { value.ffmpegString }

	public init(name: String = "", value: FFmpegStringConvertible) {
		self.name = name
		self.value = value
	}
}
