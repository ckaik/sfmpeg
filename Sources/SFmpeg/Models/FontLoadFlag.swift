import Foundation

public enum FontLoadFlag: String, FFmpegStringConvertible {
	case `default`
	case noScale = "no_scale"
	case noHinting = "no_hinting"
	case render
	case noBitmap = "no_bitmap"
	case verticalLayout = "vertical_layout"
	case forceAutohint = "force_autohint"
	case cropBitmap = "crop_bitmap"
	case pedantic
	case ignoreGlobalAdvanceWidth = "ignore_global_advance_width"
	case noRecurse = "no_recurse"
	case ignoreTransform = "ignore_transform"
	case monochrome
	case linearDesign = "linear_design"
	case noAutohint = "no_autohint"
}
