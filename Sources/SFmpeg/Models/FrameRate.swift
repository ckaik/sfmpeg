import Foundation

public enum FrameRate: String, FFmpegStringConvertible {
	case sourceFPS = "source_fps"
	case ntsc
	case pal
	case film
	case ntscFilm
}
