import Foundation

extension Array {
	static func += (lhs: inout [Element], rhs: Element) {
		lhs.append(rhs)
	}
}
