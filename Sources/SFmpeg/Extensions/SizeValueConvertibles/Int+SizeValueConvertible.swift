import Foundation

extension Int: SizeValueConvertible {
	public var sizeValue: SizeValue {
		if self == 0 {
			return .sameAsInput
		} else if self < 0 {
			return .keepingAspectRatioDivisibleBy(UInt(abs(self)))
		} else {
			return .value(UInt(self))
		}
	}
}
