import Foundation

extension String: SizeValueConvertible {
	public var sizeValue: SizeValue { .raw(self) }
}
