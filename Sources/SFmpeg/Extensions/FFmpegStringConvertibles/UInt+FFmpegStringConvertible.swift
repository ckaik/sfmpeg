import Foundation

extension UInt: FFmpegStringConvertible {
	public var ffmpegString: String { "\(self)" }
}
