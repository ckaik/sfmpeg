import Foundation

extension Bool: FFmpegStringConvertible {
	public var ffmpegString: String { self ? "1" : "0" }
}
