import Foundation

extension Double: FFmpegStringConvertible {
	public var ffmpegString: String { "\(self)" }
}
