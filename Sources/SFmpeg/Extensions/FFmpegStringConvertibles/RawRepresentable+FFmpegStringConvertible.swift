import Foundation

public extension RawRepresentable where RawValue == String {
	var ffmpegString: String { rawValue }
}
