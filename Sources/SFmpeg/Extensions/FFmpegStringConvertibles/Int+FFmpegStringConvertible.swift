import Foundation

extension Int: FFmpegStringConvertible {
	public var ffmpegString: String { "\(self)" }
}
