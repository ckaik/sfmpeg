import Foundation

extension String: FFmpegStringConvertible {
	public var ffmpegString: String { self }
}
