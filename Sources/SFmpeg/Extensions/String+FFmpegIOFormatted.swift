import Foundation

extension String {
	var ffmpegIoFormatted: String { "[\(self)]" }
}
