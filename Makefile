all: format

format:
	@echo "--> Running swiftformat"
	swiftformat .
