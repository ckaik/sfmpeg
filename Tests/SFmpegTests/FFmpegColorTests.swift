//
//  FFmpegColorTests.swift
//
//
//  Created by Christian on 22.06.22.
//
import SFmpeg
import XCTest

class FFmpegColorTests: XCTestCase {
	func testColor() throws {
		XCTAssertEqual(FFmpegColor(red: .max, green: .max, blue: .max).ffmpegString, "#FFFFFF")
		XCTAssertEqual(FFmpegColor(red: .min, green: .min, blue: .min).ffmpegString, "#000000")
		XCTAssertEqual(FFmpegColor(red: .max, green: .max, blue: .max, alpha: .min).ffmpegString, "#FFFFFF00")
		XCTAssertEqual(FFmpegColor(red: .min, green: .min, blue: .min, alpha: .min).ffmpegString, "#00000000")
		XCTAssertEqual(FFmpegColor(red: .max, green: .max, blue: .max, alpha: .max).ffmpegString, "#FFFFFFFF")
		XCTAssertEqual(FFmpegColor(red: .min, green: .min, blue: .min, alpha: .max).ffmpegString, "#000000FF")
		XCTAssertEqual(FFmpegColor(red: 15, green: 127, blue: 200, alpha: 254).ffmpegString, "#0F7FC8FE")
	}
}
